//MODULE
var proageApp = angular.module('proageApp',['ngRoute','ngResource','ngFileUpload'])
.config( [
    '$compileProvider',
    function( $compileProvider )
    {   
       $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|blob):|data:image\//);
   //     $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|blob):|data:image\//);
      
    }
]);


