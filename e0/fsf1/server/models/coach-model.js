// coach-model.js
// Creates a model for coachtbl table
// References:
// http://docs.sequelizejs.com/en/latest/docs/getting-started/#your-first-model
// http://docs.sequelizejs.com/en/latest/docs/models-definition/
module.exports = function (conn, Sequelize) {
    var CoachModel = conn.define('coach', {
            coachId: {
                type: Sequelize.STRING(30),
                allowNull: false,
                primaryKey: true
            },
            firstname: {
                type: Sequelize.STRING(30),
                allowNull: false
            },
            lastname: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            dob: {
                type: Sequelize.Date(),
                allowNull: false,
            },
            gender: {
                type: Sequelize.STRING(1),
                allowNull: false
            },
            contact: {
                type: Sequelize.STRING(15),
                allowNull: false
            },
            email: {
                type: Sequelize.STRING(50),
                allowNull: false,
            },
            nationality: {
                type: Sequelize.STRING(10),
                allowNull: true
            },
            countryorigin: {
                type: Sequelize.STRING(50),
                allowNull: true
            },
            coachtype: {
                type: Sequelize.STRING(10),
                allowNull: true
            },
            sbMessageID: {
                type: Sequelize.STRING(50),
                allowNull: true
            }
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false,
            tableName: "coachtbl"
        }
    );

    return CoachModel;
};