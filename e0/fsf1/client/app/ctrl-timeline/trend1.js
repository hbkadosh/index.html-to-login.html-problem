// Mock m1 data set
var data = {
  m1Result: [{
    "period": "w1",
    "count": "21"
  }, {
    "period": "w2",
    "count": "5"
  }, {
    "period": "w3",
    "count": "55"
  }, {
    "period": "w4",
    "count": "50"
  }, {
    "period": "w5",
    "count": "43"
  }, {
    "period": "w6",
    "count": "48"
  }, {
    "period": "w7",
    "count": "32"
  },{
    "period": "w8",
    "count": "40"
  }]
};

// 1. select <div id="graph-01">
d3.select('div#graph-01')
  // 2. declare the abstract selection of PRESENT & FUTURE rects for data binding
  .selectAll('div.bar-group')
  // 3. bind the data to the abstract selection with an identifier
  .data(data.result, function(d) {
    return d.name
  })
  // 4. state of handling NEW divs that don't match existing ones
  .enter()
  // 5. for each placeholder created by enter(), append will insert a div element
  .append('div')
  // 6. set attribute class="bar-group"
  .attr('class', 'bar-group')
  // 7. set the text inside each div
//   .text(function(d) {
//     // 8. access the data binded to each div
//     return d.name + " is " + d.temperature + " degrees"
//   });

// // simulating change in data
// data.result.pop();
// // show that mock data set now decreased to two items
// console.log(data.result);

// // NEXT STEP - HANDLE EXIT STATES
// // transition config
// var t = d3.transition()
//   .style('text-color', 'red')
//   .duration(5000)
//   .ease(d3.easeLinear);

// // 1.  select <div id="graph-01">
// d3.select('div#graph-01')

// // 2.  declare the abstract selection of PRESENT & FUTURE rects for data binding
// // 2.a select all <div class="bar-group">
//   .selectAll('div.bar-group')

// // 3.  bind the data to the abstract selection with an identifier
// // 3.a bind mock data set to div using .data()
// // 3.b pass in function(d){return d.name} as second parameter
// .data(data.result, function(d) {
//     return d.name
//   })

// // 4. get the exit state with .exit()
//  .exit()
// // 5. configure transition using .transition()
// .transition(t)
// // 6. set end state to be "opacity: 0"
// .style('opacity', 0)
// // 7. remove exiting elements with .remove()
// .remove()
