// timeline-model.js
// Creates a model for timelinecommenttbl table
// References:
// http://docs.sequelizejs.com/en/latest/docs/getting-started/#your-first-model
// http://docs.sequelizejs.com/en/latest/docs/models-definition/
module.exports = function (conn, Sequelize) {
    var TimelineModel = conn.define('timeline', {
            date: {
                type: Sequelize.Date(),
                allowNull: false,
                primaryKey: true
            commenterId: {
                type: Sequelize.STRING(30),
                allowNull: false,
                primaryKey: true
            },
            commenterName: {
                type: Sequelize.STRING(30),
                allowNull: false
            },
            comment: {
                type: Sequelize.STRING(255),
                allowNull: true
            },
            fileUpLoadID: {
                type: Sequelize.STRING(100),
                allowNull: false
            }
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false,
            tableName: "timelinecommenttbl"
        }
    );

    return TimelineModel;
};