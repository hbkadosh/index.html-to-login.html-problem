// login-ctrl.js

(function () {
    angular
        .module("CWS")
        .controller("LoginCtrl", ["$state", "AuthFactory", "Flash", 'ngProgressFactory', LoginCtrl]);

    function LoginCtrl($state, AuthFactory, Flash, ngProgressFactory){
        var vm = this;

        vm.progressbar = ngProgressFactory.createInstance();

        vm.login = function () {
            vm.progressbar.start();
            AuthFactory.login(vm.user)
                .then(function () {
                    if(AuthFactory.isLoggedIn()){
                        vm.id = "";                      // fsf
                        vm.emailAddress = "";
                        vm.password = "";
                        vm.progressbar.complete();
                        $state.go("timeline");
                    }else{
                        Flash.create('danger', "There's an issue logging in", 0, {class: 'custom-class', id: 'custom-id'}, true);
                        vm.progressbar.stop();
                        $state.go("login");
                    }
                }).catch(function () {
                vm.progressbar.stop();
                console.error("Error logging on !");
            });
        };
    }
})();