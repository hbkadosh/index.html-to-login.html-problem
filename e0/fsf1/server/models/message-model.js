// message-model.js
// Creates a model for messagetbl table
// References:
// http://docs.sequelizejs.com/en/latest/docs/getting-started/#your-first-model
// http://docs.sequelizejs.com/en/latest/docs/models-definition/
module.exports = function (conn, Sequelize) {
    var MessageModel = conn.define('message', {
            date: {
                type: Sequelize.Date(),
                allowNull: false,
                primaryKey: true
            messagerId: {
                type: Sequelize.STRING(30),
                allowNull: false,
                primaryKey: true
            },
            messagerName: {
                type: Sequelize.STRING(30),
                allowNull: false
            },
            message: {
                type: Sequelize.STRING(255),
                allowNull: true
            },
            fileUpLoadID: {
                type: Sequelize.STRING(100),
                allowNull: false
            }
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false,
            tableName: "messagetbl"
        }
    );

    return MessageModel;
};